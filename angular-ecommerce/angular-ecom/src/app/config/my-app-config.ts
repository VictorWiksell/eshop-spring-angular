export default {

    oidc: {
        clientId: '0oa7h06m805JliPxk5d7',
        issuer: 'https://dev-93106320.okta.com/oauth2/default',
        redirectUri: 'http://localhost:4200/login/callback',
        scopes: ['openid', 'profile', 'email']
    }
}
